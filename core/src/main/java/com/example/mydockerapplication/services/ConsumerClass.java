package com.example.mydockerapplication.services;

import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Profile("!test")
@Service
public class ConsumerClass {
    @KafkaListener(topics = "test_topic", groupId = "group_id")
    public void consumeMessage(String message) {
        System.out.println(message);
    }
}
