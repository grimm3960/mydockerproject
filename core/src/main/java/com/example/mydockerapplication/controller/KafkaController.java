package com.example.mydockerapplication.controller;

import com.example.mydockerapplication.services.ProducerClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Profile("!test")
@RestController
public class KafkaController {

    private final ProducerClass producer;

    @Autowired
    public KafkaController(ProducerClass producerClass) {
        this.producer = producerClass;
    }

    @PostMapping("/kafka")
    public void messageToTopic(@RequestParam("message") String message) {
        this.producer.sendMessage(message);
    }
}
