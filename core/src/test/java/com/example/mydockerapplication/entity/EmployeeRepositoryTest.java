package com.example.mydockerapplication.entity;

import com.example.mydockerapplication.DockerApplication;
import com.example.mydockerapplication.config.ContainersEnvironment;
import com.example.mydockerapplication.model.Employee;
import com.example.mydockerapplication.repository.EmployeeRepository;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = DockerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeRepositoryTest extends ContainersEnvironment {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    @Transactional
    @Order(1)
    public void When_Employee_From_Db_Equals_Custom_Employee() {

        //GIVEN
        Employee employee1 = employeeRepository.save(new Employee(1L,
                "Adrian",
                "Mkhitarov",
                "gmail.ru"));
        Employee employee2 = employeeRepository.save(new Employee(1L,
                "Adrian2",
                "Mkhitarov2",
                "gmail.ru2"));

        //WHEN
        Employee savedEmployee1 = employeeRepository.getReferenceById(employee1.getId());
        Employee savedEmployee2 = employeeRepository.getReferenceById(employee2.getId());

        //THAT
        assertEquals(employee1, savedEmployee1);
        assertEquals(employee2, savedEmployee2);
    }

    @Test
    @Order(2)
    public void shouldStoreEachNotification() {

        // GIVEN
        employeeRepository.save(new Employee(2L, "adrian", "mkhitarov", "adrian@gmail.com"));
        employeeRepository.save(new Employee(3L, "Andrew", "Andrew", "Andrew@gmail.com"));

        // WHEN
        long count = employeeRepository.count();

        // THEN
        assertThat(count, is(2L));
    }

    @Test
    @Order(3)
    public void When_GetEmployees_Expect_EmptyList() {
        List<Employee> list = employeeRepository.findAll();
        assertEquals(2, list.size());
    }
}
