package com.example.mydockerapplication.entity;

import com.example.mydockerapplication.DockerApplication;
import com.example.mydockerapplication.config.ContainersEnvironment;
import com.example.mydockerapplication.model.Product;
import com.example.mydockerapplication.repository.ProductRepository;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = DockerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductRepositoryTest extends ContainersEnvironment {

    @Autowired
    private ProductRepository productRepository;

    @Test
    @Transactional
    @Order(1)
    public void When_Employee_From_Db_Equals_Custom_Employee() {

        //GIVEN
        Product employee1 = productRepository.save(new Product(1L, "Adrian"));
        Product employee2 = productRepository.save(new Product(1L, "Adrian2"));

        //WHEN
        Product savedEmployee1 = productRepository.getReferenceById(employee1.getId());
        Product savedEmployee2 = productRepository.getReferenceById(employee2.getId());

        //THAT
        assertEquals(employee1, savedEmployee1);
        assertEquals(employee2, savedEmployee2);
    }

    @Test
    @Order(2)
    public void shouldStoreEachNotification() {

        // GIVEN
        productRepository.save(new Product(2L, "adrian"));
        productRepository.save(new Product(3L, "Andrew"));

        // WHEN
        long count = productRepository.count();

        // THEN
        assertThat(count, is(2L));
    }

    @Test
    public void When_GetProducts_Expect_EmptyList(){
        List<Product> list = productRepository.findAll();
        assertEquals(2, list.size());
    }
}
